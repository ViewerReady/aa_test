﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(_Renderer))]
public class RendererEditor : Editor {
    _Renderer t;
    private void OnEnable()
    {
        t = (_Renderer)target;
    }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Render Unlit"))
            t.RenderUnlit();
        if (GUILayout.Button("Render Lit"))
            t.RenderLit();
        if (GUILayout.Button("Render Wireframe"))
            t.RenderWireframe();
        if (GUILayout.Button("Render Shaded-Wireframe"))
            t.RenderWireframeShaded();
        EditorUtility.SetDirty(t);
    }
}
