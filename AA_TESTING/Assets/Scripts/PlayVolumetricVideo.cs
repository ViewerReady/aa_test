using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayVolumetricVideo : MonoBehaviour
{

    [SerializeField]
    private MegaCacheOBJ objCache;

    [SerializeField]
    private MediaPlayerCtrl texture;

    private void Start()
    {
        StartCoroutine(AsyncStart());
    }

    [ContextMenu("StartVideo")]
    private void StartVid()
    {
        StartCoroutine(AsyncStart());
    }

    private IEnumerator AsyncStart()
    {
        yield return new WaitForSeconds(0.5f);
        DoIt();
    }

    private void DoIt()
    {
        texture.OnVideoFirstFrameReady += Ready;
        texture.Play();
    }

    private void Ready()
    {
        texture.OnVideoFirstFrameReady -= Ready;
    }

    public float seekPosition = 0;

    float prevSeekPosition = -1;

    float framerate = 15f;

    public int frame = 0;

    public int frameOffset = -3;

    private void Update()
    {
        seekPosition = (float)texture.GetSeekPosition() / 1000f;
        if(prevSeekPosition > 0)
        {
            frame = Mathf.CeilToInt((seekPosition + Time.deltaTime) / (1f / framerate));
            objCache.frame = Mathf.Clamp(frame+frameOffset, 0, objCache.framecount);
        }
        prevSeekPosition = seekPosition;
        
    }

    /*
    private void FrameUpdated()
    {
        objCache.frame++;
        objCache.frame = Mathf.Clamp(objCache.frame, 0, objCache.framecount);
    }
    */

}
