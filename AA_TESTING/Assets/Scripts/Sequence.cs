﻿using UnityEngine;
using System.IO;
using Pcx;
using UnityEngine.VFX;
using System.Linq;
using System;
using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;

[Serializable]
public class Sequence : Playable {
    public Material VertexColorMat;
    private MeshFilter mf;
    private MeshRenderer mr;
    private VisualEffect _vfx;
    public _Renderer or;
    private bool isObj, isMesh = true;
    private int f = -1, preloadId;
    public string path;
    private object preload;
    public string[] files;
    public int generatedFrames;
    private VisualEffect vfx
    {
        get { if (_vfx == null) _vfx = gameObject.AddComponent<VisualEffect>(); return _vfx; }
    }
    [SerializeField] Texture2D _colorTexture2D = null;
    [SerializeField] Texture2D _positionTexture2D = null;
    private static string tcp = "";
    private static string framePath { get { if (tcp == "") tcp = Application.temporaryCachePath + "/PFS"; return tcp; } }
    
    public IEnumerator LoadSequence(string path)
    {
        Pause();
        this.path = path;
        transform.DestroyChildren();
        files = Directory.GetFiles(path, "*.obj");
        isObj = files.Length > 0;
        //if (isObj && !isMesh) return;
        if (!isObj) files = Directory.GetFiles(path, "*.ply");
        files = files.OrderBy(x => x.getNumbersAtEnd()).ToArray();
        if (files.Length == 0) yield break;
        Stop();
        yield return LoadFrames();
    }
    private IEnumerator LoadFrames()
    {
        framePath.ResetDirectory();
        preloadId = 0;
        preload = new object();
        List<Task> tasks = new List<Task>();
        int taskCount = files.Length;
        foreach (var f in files) tasks.Add(Task.Run(PreLoad));
        while (tasks.Count > 0)
            for (int i = 0; i < tasks.Count; i++)
            {
                yield return null;
                if (!tasks[i].isRunning())
                {
                    tasks.RemoveAt(i);
                    i--;
                    generatedFrames++;
                }
            }
        FinishLoad();
    }
    private void PreLoad()
    {
        try
        {
            int i;
            string file;
            lock (preload) i = preloadId++;
            lock (files) file = files[i];
            while (!file.IsFileReady()) { }
            if (isObj) new Dummiesman.OBJLoader().PreLoad(file, GetPathFaces(i), GetPathPosition(i), GetPathColor(i), GetPathNormals(i), GetPathUVS(i));
            else
            {
                var data = new PlyImporter().Import(file);
                isMesh = data.faces.Length > 0;
                if (isMesh) new Dummiesman.PLYLoader().PreLoad(data, GetPathFaces(i), GetPathPosition(i), GetPathColor(i), GetPathNormals(i), GetPathUVS(i));
                else new PlyImporter().PreLoad(data, GetPathPosition(i), GetPathColor(i));
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }
    private void FinishLoad()
    {
        try
        {
            or = transform.GetComponentInParent<_Renderer>();
            if (isMesh) or.RenderUnlit();
            else StartVFX();
            Play();
            LoadFrame(-1, currentFrame);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }
    public void ImportMesh(GameObject mesh)
    {
        GameObject child = Instantiate(mesh, transform);
        DestroyImmediate(mesh);
        child.gameObject.SetActive(false);
        MeshRenderer meshRenderer = child.GetComponentInChildren<MeshRenderer>();
        meshRenderer.sharedMaterial = new Material(meshRenderer.sharedMaterial);
    }
    public void StartVFX()
    {
        vfx.pause = false;
        or.VFXFilter(0);
        vfx.Play();
    }

    private void OnBecameVisible()
    {
        _Update();
    }
    private void _Update()
    {
        if (!isMesh)
        {
            if (vfx.HasTexture("Position Map")) vfx.SetTexture("Position Map", _positionTexture2D);
            if (vfx.HasTexture("Color Map")) vfx.SetTexture("Color Map", _colorTexture2D);
            if (vfx.HasFloat("Size")) vfx.SetFloat("Size", or.pointSize * or.mul);
        }
    }
    private void ResetPointcloud()
    {
        _positionTexture2D = new Texture2D(0, 0, TextureFormat.RGBAHalf, false);
        _positionTexture2D.name = "Position Map";
        _positionTexture2D.filterMode = FilterMode.Point;

        _colorTexture2D = new Texture2D(0, 0, TextureFormat.RGBA32, false);
        _colorTexture2D.name = "Color Map";
        _colorTexture2D.filterMode = FilterMode.Point;

        _positionTexture2D.Apply(false, true);
        _colorTexture2D.Apply(false, true);

        _Update();
    }
    private string GetPathColor(int id)
    {
        return framePath + "/" + id + "C.dat";
    }
    private string GetPathPosition(int id)
    {
        return framePath + "/" + id + "P.dat";
    }
    private string GetPathNormals(int id)
    {
        return framePath + "/" + id + "N.dat";
    }
    private string GetPathUVS(int id)
    {
        return framePath + "/" + id + "U.dat";
    }
    private string GetPathFaces(int id)
    {
        return framePath + "/" + id + "F.dat";
    }
    //Playback methods
    public override void Stop()
    {
        base.Stop();
        if (isMesh && transform.childCount > 0) transform.GetChild(0).gameObject.SetActive(false);
        else ResetPointcloud();
    }
    public override void Play()
    {
        if (isMesh && transform.childCount > 0) transform.GetChild(0).gameObject.SetActive(true);
        else _Update();
        base.Play();
    }
    public override void Pause()
    {
        if (isMesh && transform.childCount > 0) transform.GetChild(0).gameObject.SetActive(true);
        else _Update();
        base.Pause();
    }
    public override int GetMaxFrames()
    {
        return files.Length;
    }

    public Material pleaseWork;
    public override bool LoadFrame(float lastFrame, float newFrame)
    {
        if (base.LoadFrame(lastFrame, newFrame))
        {
            int id = Mathf.FloorToInt(newFrame);
            if (!isMesh)
            {
                f = id;
                _positionTexture2D?.Destroy();
                _colorTexture2D?.Destroy();

                VFXTexture.LoadFromFiles(out _positionTexture2D, out _colorTexture2D, GetPathPosition(id), GetPathColor(id));
                _Update();
            }
            else
            {
                transform.DestroyChildren();
                GameObject go;

                if (isObj) go = new Dummiesman.OBJLoader().Load(files[id], GetPathFaces(id), GetPathPosition(id), GetPathColor(id), GetPathNormals(id), GetPathUVS(id), pleaseWork);
                else go = new Dummiesman.PLYLoader().Load(files[id], GetPathFaces(id), GetPathPosition(id), GetPathColor(id), GetPathNormals(id), GetPathUVS(id));
                go.transform.SetParent(transform);
                go.transform.position = Vector3.zero;
                go.transform.localScale = new Vector3(1f, 1f, 1f);
                or.Render();
            }
            return true;
        }
        return false;
    }
}