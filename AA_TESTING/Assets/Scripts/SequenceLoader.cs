﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

public class SequenceLoader : MonoBehaviour {
    public GameObject sequencePrefab;

    public string sequencePath = "";
    public bool isGenerating = false;
    private Renderer r;
    [HideInInspector]
    public Sequence sequence;

#if UNITY_EDITOR
    public void SelectSequencePath()
    {
        sequencePath = UnityEditor.EditorUtility.OpenFolderPanel("Choose your Obj Sequence location", Application.dataPath, "");
    }
#endif

    public void GenerateFrames()
    {
        isGenerating = true;
        DeletePreviousFrames();
        StartCoroutine(IGenerateFrames());
    }

    public void DeletePreviousFrames()
    {
        transform.GetChild(0).DestroyChildren();
    }

    public IEnumerator IGenerateFrames()
    {
        sequence = sequencePrefab.Instantiate<Sequence>(transform.GetChild(0));
        yield return sequence.LoadSequence(sequencePath);
        isGenerating = false;
    }
}