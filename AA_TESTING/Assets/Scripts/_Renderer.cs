﻿using System;
using UnityEngine;
using UnityEngine.VFX;

public class _Renderer : MonoBehaviour {
    public static _Renderer I { get; private set; }
    private void Awake() { if (I != null && I != this) Destroy(gameObject); else I = this; }
    public Shader Unlit, Lit, Wireframe, WireframeShaded, PointCloud, Triangle;
    public VisualEffectAsset[] vfxFilter;
    public Color WireframeColor;
    private bool setWireframe;
    public float smoothness, pointSize = 20, mul = .5f;
    [HideInInspector]
    public Shader currentShader;
    private int currentVfx = -1;
    private Transform currentParent;
    public int pointLimit = 0;
    
    private void Start()
    {
        currentParent = transform.GetChild(0);
        RenderUnlit();
    }

    #region Rendering
    public void RenderUnlit()
    {
        Render(Unlit, action: x => {
            x.SetFloat("_CullMode", 1);
        });
    }
    public void RenderLit()
    {
        Render(Lit, action: x =>
        {
            x.SetFloat("_CullMode", 1);
        });
    }
    public void RenderPointCloud(float size)
    {
        Render(PointCloud, action: x => { x.SetFloat("_PointSize", size); });
    }
    public void RenderWireframe()
    {
        smoothness = 3;
        Render(Wireframe, true);
    }
    public void RenderWireframeShaded()
    {
        smoothness = 16;
        Render(WireframeShaded, true);
    }
    private void Render(Shader s, bool wireframe = false, Action<Material> action = null)
    {
        setWireframe = wireframe;
        currentShader = s;
        Render(action);
    }
    public void Render(Action<Material> action = null)
    {
        for (int p = 0; p < transform.childCount; p++)
        {
            var c = transform.GetChild(p);
            for (int i = 0; i < c.childCount; i++)
            {
                var sequence = c.GetChild(i);
                for (int o = 0; o < sequence.childCount; o++)
                {
                    var mat = sequence.GetChild(o).GetComponentInChildren<MeshRenderer>().sharedMaterial;
                    Texture t = null;
                    if (mat.HasProperty("_MainTex")) t = mat.GetTexture("_MainTex");
                    mat.shader = currentShader;
                    if (t != null)
                    {
                        action?.Invoke(mat);
                        if (mat.HasProperty("_BaseMap")) mat.SetTexture("_BaseMap", t);
                        if (mat.HasProperty("_UnlitColorMap")) mat.SetTexture("_UnlitColorMap", t);
                        if (mat.HasProperty("_BaseColorMap")) mat.SetTexture("_BaseColorMap", t);
                    }
                    if (setWireframe) _SetWireframe(mat);
                }
            }
        }
    }
    private void _SetWireframe(Material mat)
    {
        mat.SetFloat("_WireThickness", 500);
        mat.SetFloat("_WireSmoothness", smoothness);
        mat.SetColor("_WireColor", WireframeColor);
        mat.SetColor("_BaseColor", Color.white);
        mat.SetFloat("_MaxTriSize", 200);
    }
    #endregion

    #region VFX
    public void VFXFilter(int id)
    {
        if (vfxFilter.Length > id && id >= 0 && vfxFilter[id] != null)
        {
            currentVfx = id;
            if (id == 0 || id == 8) VFXFilter(vfxFilter[id], x => x.SetFloat("Size", pointSize * mul));
            else VFXFilter(vfxFilter[id]);
        }
    }
    public void VFXFilter(VisualEffectAsset vfx, Action<VisualEffect> action = null)
    {
        for (int p = 0; p < transform.childCount; p++)
        {
            var c = transform.GetChild(p);
            for (int i = 0; i < c.childCount; i++)
            {
                var ve = c.GetChild(i).GetComponent<VisualEffect>();
                if (ve != null)
                {
                    ve.visualEffectAsset = vfx;
                    action?.Invoke(ve);
                }
            }
        }
    }
    public void SetPointMultiplier(float m)
    {
        mul = m;
        VFXFilter(currentVfx);
    }
    public void SetPointSize(float size)
    {
        pointSize = size;
        VFXFilter(currentVfx);
    }
    public void PointSizeRelativeToScale(float scale)
    {
        SetPointSize((1 - Mathf.Clamp(scale, 0, 2000) / 2000) * 10 + 10);
    }
    #endregion
}