﻿using System;
using System.Collections;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

public static class VFXTexture {
    public static int PointcloudToTexture(out Texture2D positionTexture, out Texture2D colorTexture, float[] pointData, byte[] rgbData, int everyNth = 3, int pointLimit = 0)
    {
        return PointcloudToTexture(out positionTexture, out colorTexture, pointData.Length, 3, rgbData, everyNth, pointLimit, false);
    }
    public static int PointcloudToTexture(out Texture2D positionTexture, out Texture2D colorTexture, System.Numerics.Vector3[] pointData, byte[] rgbData, int everyNth = 1, int pointLimit = 0)
    {
        getX = x => pointData[x].X;
        getY = x => pointData[x].Y;
        getZ = x => pointData[x].Z;
        VFXTexture.pointData = pointData;
        return PointcloudToTexture(out positionTexture, out colorTexture, pointData.Length, 1, rgbData, everyNth, pointLimit, true);
    }
    private static System.Numerics.Vector3[] pointData;
    private static void test(int i)
    {
        if (i >= _pointCount)
        {
            i2 += 132049U; // prime
            i = (int)(i2 % _pointCount);
        }
        int ie = i * enth;

        pColors[i] = new Color(
            pointData[ie].X,
            pointData[ie].Y,
            pointData[ie].Z
            );

        ie *= 3;
        cColors[i] = new Color(
            rgbData[ie + 2] / 255f,
            rgbData[ie + 1] / 255f,
            rgbData[ie] / 255f);
    }
    private static void test2(int i)
    {
        for (int o = 0; o < width; o++) test(i * width + o);
    }
    private static uint i2;
    private static Color[] pColors, cColors;
    private static byte[] rgbData;
    private static int _pointCount, enth, mof, width;
    private static Func<int, float> getX, getY, getZ;
    public static int PointcloudToTexture(out Texture2D positionTexture, out Texture2D colorTexture, int dataLength, int multipleOf, byte[] rgbData, int everyNth, int pointLimit, bool readable)
    {
        mof = multipleOf;
        enth = everyNth;
        VFXTexture.rgbData = rgbData;
        try
        {
            _pointCount = dataLength / enth;
            //Figure out how many points should there be in the final product
            if (pointLimit > 0 && _pointCount > pointLimit)
            {
                _pointCount = pointLimit;
                if (mof != 1) _pointCount += mof - pointLimit % mof;
                if (dataLength / mof < _pointCount) enth = mof;
                else
                {
                    enth = dataLength / _pointCount;
                    if (mof != 1 && enth % mof != 0) enth += mof - enth % mof;
                }
                _pointCount = dataLength / enth;
            }

            width = Mathf.CeilToInt(Mathf.Sqrt(_pointCount));

            //Create color arrays for textures
            pColors = new Color[width * width];
            cColors = new Color[width * width];

            //Create the textures
            var pt = new Texture2D(width, width, TextureFormat.RGBAHalf, false);
            pt.name = "Position Map";
            pt.filterMode = FilterMode.Point;

            var ct = new Texture2D(width, width, TextureFormat.RGBA32, false);
            ct.name = "Color Map";
            ct.filterMode = FilterMode.Point;

            //Fill color arrays for textures
            i2 = 0U;
            Parallel.For(0, width, i => test2(i));

            //Assign color arrays to textures
            pt.SetPixels(pColors);
            ct.SetPixels(cColors);

            //Reset arrays and clear memory
            pColors = null;
            cColors = null;
            Task.Run(() => GC.Collect(0));

            //Save changed colors on textures
            pt.Apply(false, !readable);
            ct.Apply(false, !readable);

            positionTexture = pt;
            colorTexture = ct;
            return _pointCount;
        }
        catch (Exception e)
        {
            Debug.Log(e);
            positionTexture = null;
            colorTexture = null;
            return 0;
        }
    }
    public static int LoadFromFiles(out Texture2D positionTexture, out Texture2D colorTexture, string ptPath, string ctPath, bool readable = false, int pointLimit = 0)
    {
        if (!File.Exists(ptPath) || !File.Exists(ctPath))
        {
            positionTexture = new Texture2D(0, 0, TextureFormat.RGBAHalf, false);
            positionTexture.name = "Position Map";
            positionTexture.filterMode = FilterMode.Point;

            colorTexture = new Texture2D(0, 0, TextureFormat.RGBA32, false);
            colorTexture.name = "Color Map";
            colorTexture.filterMode = FilterMode.Point;
            return 0;
        }
        var p = File.ReadAllBytes(ptPath).ToNumericVector3Array();
        var c = File.ReadAllBytes(ctPath);

        int i = PointcloudToTexture(out positionTexture, out colorTexture, p, c, pointLimit: pointLimit);
        return i;
    }
    public static Texture2D pt, ct;
    public static IEnumerator ILoadFromFiles(string ptPath, string ctPath, bool readable = false, int pointLimit = 0)
    {
        if (!File.Exists(ptPath) || !File.Exists(ctPath))
        {
            pt = new Texture2D(0, 0, TextureFormat.RGBAHalf, false);
            pt.name = "Position Map";
            pt.filterMode = FilterMode.Point;

            ct = new Texture2D(0, 0, TextureFormat.RGBA32, false);
            ct.name = "Color Map";
            ct.filterMode = FilterMode.Point;
            yield break;
        }

        System.Numerics.Vector3[] p = File.ReadAllBytes(ptPath).ToNumericVector3Array();
        byte[] c = File.ReadAllBytes(ctPath);

        PointcloudToTexture(out pt, out ct, p, c, pointLimit: pointLimit);
    }
}