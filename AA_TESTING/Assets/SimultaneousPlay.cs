﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class SimultaneousPlay : MonoBehaviour
{
    public VideoPlayer vp;
    public MegaCacheOBJ obj;
    private long originalID = -100;

    private void Awake()
    {
        vp.sendFrameReadyEvents = true;
        vp.frameReady += PauseWhenFrameReady;
    }

    void PauseWhenFrameReady(UnityEngine.Video.VideoPlayer source, long frameIDx)
    {
        if(originalID == -100)
        {
            Debug.Log("Setting original ID");
            originalID = frameIDx;
        }
        Debug.Log("New frame idx: " + frameIDx);
        obj.frame = (int)(frameIDx - originalID);
        Debug.Break();
    }

    [ContextMenu("Play Sequence")]
    void Play()
    {
        vp.Play();
        obj.animate = true;
    }

    private void Update()
    {
        Debug.Log("----------------");
        Debug.Log("Current VP frame: " + vp.frame);
        Debug.Log("Current OBJ frame: " + obj.frame);
        Debug.Log("Max VP frames: " + vp.frameCount);
        Debug.Log("VP framerate: " + vp.frameRate);
        Debug.Log("----------------");
    }

    [ContextMenu("Set first frame")]
    void SetFirstFrame()
    {
        vp.Pause();
        obj.animate = false;
        vp.frame = 0;
        obj.frame = 0;
        Debug.Log("VP now at frame: " + vp.frame);
        Debug.Log("OBJ now at frame: " + obj.frame);
    }

    [ContextMenu("Incrememnt Frame")]
    void IncrementFrame()
    {
        vp.Pause();
        obj.animate = false;
        vp.frame = vp.frame + 1;
        obj.frame = obj.frame + 1;
        Debug.Log("VP now at frame: " + vp.frame);
        Debug.Log("OBJ now at frame: " + obj.frame);
    }
}
